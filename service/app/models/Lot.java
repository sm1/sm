package models;

import play.db.ebean.Model;
import play.data.format.*;

import util.Helper;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Column;

import javax.validation.constraints.NotNull;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.util.List;

import com.avaje.ebean.PagingList;

@Entity
public class Lot extends Model {

    public static final String FIELD_ID = "id";
    public static final String FIELD_OWNER = "owner";
    public static final String FIELD_ACCOUNT = "account";
    public static final String FIELD_MEMBERS = "members";
    public static final String FIELD_MIN = "min";
    public static final String FIELD_START = "start";
    public static final String FIELD_END = "end";

    @Id
    public Long id;

    @ManyToOne
    @Column(nullable=false) 
    public User owner;

    @NotNull
    @Column(nullable=false) 
    public Long account;

    @NotNull
    @Column(nullable=false)
    public Long min;

    @NotNull
    @Column(nullable=false)
    @Formats.DateTime(pattern="dd/MM/yyyy")
    public Date start;

    @NotNull
    @Column(nullable=false)
    @Formats.DateTime(pattern="dd/MM/yyyy")
    public Date end;

    @ManyToMany
    public Set<User> members;

    public static final Finder<Long, Lot> find = new Finder<Long, Lot>(Long.class, Lot.class);

    public static PagingList<Lot> allPages(int size) {
        return find.findPagingList(size);
    }
    
    public static Lot findById(String id) {
        return find.where().eq(FIELD_ID, id).findUnique(); 
    }

    public Set<User> getMembers() {
	return members;
    }

    public User getOwner() {
	return owner;
    }

}
