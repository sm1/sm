package models;

import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Role;
import be.objectify.deadbolt.core.models.Subject;

import play.db.ebean.Model;

import util.Helper;

import play.db.ebean.Model.Finder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.UniqueConstraint;
import javax.persistence.Column;

import javax.validation.constraints.NotNull;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import com.avaje.ebean.PagingList;

@Entity
public class User extends Model implements Subject {

    public static final String FIELD_LOGIN = "login";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_PASSWORD = "password";
    public static final String FIELD_ACCOUNT = "account";
    public static final String FIELD_IP = "ip";
    public static final String FIELD_ROLE = "role";
    public static final String FIELD_ID = "id";
    public static final String FIELD_HASH = "hash";


    @Id
    public Long id;

    @NotNull
    @Column(unique=true, nullable=false) 
    public String login;

    @NotNull
    @Column(unique=true, nullable=false) 
    public String email;

    @NotNull
    @Column(nullable=false) 
    public String password;

    public String hash;

    @NotNull
    @Column(nullable=false) 
    public Long account;

    @ManyToMany
    public List<SecurityRole> roles;

    public String ip;

    @OneToMany(mappedBy = "owner")
    public List<Lot> owner_lots;

    @ManyToMany(mappedBy = "members")
    public Set<Lot> member_lots;

    public static final Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);

    public Long getId() {
        return id;
    }

    public Long getAccount() {
        return account;
    }
   
    @Override
    public List<? extends Role> getRoles() {
        return roles;
    }

    @Override
    public List<? extends Permission> getPermissions() {
        return Collections.emptyList();
    }

    public String getLogin() {
	return login;
    }

    @Override
    public String getIdentifier() {
        return login;
    }

    public List<Lot> getOwner_lots() {
	return owner_lots;
    }

    public Set<Lot> getMember_lots() {
	return member_lots;
    }

    public static PagingList<User> allPages(int pageSize) {
	return find.findPagingList(pageSize);
    }

    public static User findById(String id) {
        return find.where().eq(FIELD_ID, id).findUnique();
    }

    public static User findByLogin(String login) {
        return find.where().eq(FIELD_LOGIN, login).findUnique();
    }

    public static User findByEmail(String email) {
        return find.where().eq(FIELD_EMAIL, email).findUnique();
    }
    public static User findByLoginAndPassword(String login, String password) {
        return find.where().eq(FIELD_LOGIN, login).eq(FIELD_PASSWORD, Helper.getDoubleMD5(password)).findUnique();
    }

    public static User findByIdAndHash(String id, String hash) {
        return find.where().eq(FIELD_ID, id).eq(FIELD_HASH, hash).findUnique();
    }

    public static User findByIdAndHashAndIp(String id, String hash, String ip) {
        return find.where().eq(FIELD_ID, id).eq(FIELD_HASH, hash).eq(FIELD_IP, ip).findUnique();
    }

}
