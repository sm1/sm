import com.avaje.ebean.Ebean;
import models.User;
import models.SecurityRole;
import play.Application;
import play.GlobalSettings;

import java.util.ArrayList;
import java.util.Arrays;

public class Global extends GlobalSettings
{
    @Override
    public void onStart(Application application)
    {

	if (User.find.findRowCount() == 0)
        {
            User user = new User();
            user.login = "iceberg";
            user.password = "b1520bfe7e0b2d4415900242c1f932df";
            user.email = "cromlehg@gmail.com";
            user.account = new Long(0);
            user.roles = new ArrayList<SecurityRole>();
            user.roles.add(SecurityRole.getOrCreateRole(models.SecurityRole.ROLE_ADMIN));

            user.save();
            Ebean.saveManyToManyAssociations(user, "roles");
        }

    }
}
