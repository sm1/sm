package controllers;

import play.data.Form;
import play.data.validation.Constraints.Required;
import play.i18n.Messages;

import models.User;

public class LoginUserForm {

	@Required(message = "validation.required.login")
        public String login;

	@Required(message = "validation.required.password")
        public String password;

	public String validate() {
           User user = User.findByLoginAndPassword(login, password);
           if (user == null) {
	       return Messages.get("form.login.validation.invalid_user_or_pass");
           }
	   return null;
	}

}

