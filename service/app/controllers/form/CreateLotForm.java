package controllers;

import play.data.Form;
import play.data.validation.Constraints.Required;
import play.i18n.Messages;

import java.util.Date;
import java.util.Calendar;

public class CreateLotForm {

	@Required(message = "validation.required.min")
        public String min;

	@Required(message = "validation.required.start")
        public Date start;

	@Required(message = "validation.required.end")
        public Date end;

	public String validate() {
	   try {
               Long.parseLong(min.trim());
	   } catch (NumberFormatException e) {
	       return Messages.get("form.create_lot.validation.invalid_min");
           }

           Date currentDate = Calendar.getInstance().getTime();
           if(!start.after(currentDate)) {
	       return Messages.get("form.create_lot.validation.invalid_cur_after_start");
           }	   
           
	   if(!end.after(start)) {
	       return Messages.get("form.create_lot.validation.invalid_end_after_start");
           }
	   
	   return null;
	}

}
