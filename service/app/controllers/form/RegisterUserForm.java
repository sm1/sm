package controllers;

import play.data.Form;
import play.data.validation.Constraints.Required;
import play.data.validation.Constraints.Email;
import play.i18n.Messages;

import models.User;

public class RegisterUserForm {

	@Required(message = "validation.required.login")
        public String login;

	@Email(message = "validation.format.email")
	@Required(message = "validation.required.email")
        public String email;

	@Required(message = "validation.required.password")
        public String password;

	@Required(message = "validation.required.repassword")
        public String repassword;

	public String validate() {
           User user = User.findByLogin(login);
           if (user != null) {
	       return Messages.get("form.register.validation.login_exists");
           }

           user = User.findByEmail(email);
	   if (user != null) {
	       return Messages.get("form.register.validation.email_exists");
           }

	   if(!password.equals(repassword)) {
	       return Messages.get("form.register.validation.pass_equals");
	   }
	   return null;
	}

}

