package controllers;

import play.data.Form;
import play.data.validation.Constraints.Required;
import play.i18n.Messages;

public class FillAccountForm {

	@Required(message = "validation.required.account")
        public String account;

	public String validate() {
	   Long lAccount = null;
	   try {
               lAccount = Long.parseLong(account.trim());
	   } catch (NumberFormatException e) {
	       return Messages.get("form.create_lot.validation.invalid_account");
           }

	   // TODO: validation

	   return null;
	}

}
