package controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import play.*;
import play.mvc.*;
import play.data.Form;
import play.i18n.Messages;

import views.html.*;

import models.*;

import util.Helper;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import be.objectify.deadbolt.java.actions.SubjectNotPresent;
import be.objectify.deadbolt.java.actions.Restrict;

import com.avaje.ebean.PagingList;
import com.avaje.ebean.Page;

public class Application extends Controller {

    private final static Form<LoginUserForm>    sLoginUserForm    = Form.form(LoginUserForm.class);
    private final static Form<RegisterUserForm> sRegisterUserForm = Form.form(RegisterUserForm.class);
    private final static Form<CreateLotForm>    sCreateLotForm    = Form.form(CreateLotForm.class);
    private final static Form<FillAccountForm>  sFillAccountForm  = Form.form(FillAccountForm.class);

    public static Result index() {
	return ok(index.render(getUser()));
    }

    public static User getUser() {
	String id = session().get(User.FIELD_ID);
	String hash = session().get(User.FIELD_HASH);

	if(id == null ||  hash == null)
		return null;

	return User.findByIdAndHash(id, hash);
    }

    @SubjectNotPresent
    public static Result register() {
	return ok(register.render(sRegisterUserForm));
    }

    @SubjectNotPresent
    public static Result addUser() {
	Form<RegisterUserForm> registerUserForm = sRegisterUserForm.bindFromRequest();
        if (registerUserForm.hasErrors()) {
            return badRequest(register.render(registerUserForm));
        }

	User user = new User();
	user.login = registerUserForm.field(User.FIELD_LOGIN).value();
	user.email = registerUserForm.field(User.FIELD_EMAIL).value();
	user.password = Helper.getDoubleMD5(registerUserForm.field(User.FIELD_PASSWORD).value());
	user.roles = new ArrayList<SecurityRole>();
	user.roles.add(SecurityRole.getOrCreateRole(SecurityRole.ROLE_USER));
	user.account = new Long(0);
	user.save();

	return redirect(routes.Application.login());
    }

    @SubjectNotPresent
    public static Result login() {
	return ok(login.render(sLoginUserForm));
    }

    @SubjectNotPresent
    public static Result auth() {
        Form<LoginUserForm> loginUserForm = sLoginUserForm.bindFromRequest();
        if (loginUserForm.hasErrors()) {
            return badRequest(login.render(loginUserForm));
        }

	User user = User.findByLogin(loginUserForm.field(User.FIELD_LOGIN).value());
	user.hash = Helper.getMD5RandomString();
	user.ip = request().remoteAddress();
	user.update(); 

        session().put(User.FIELD_ID, user.id.toString());
        session().put(User.FIELD_HASH, user.hash);
	
        return redirect(routes.Application.profile());
    }

    @SubjectPresent
    public static Result fillAccount() {
	return ok(fillAccount.render(getUser(), sFillAccountForm));
    }

    @SubjectPresent
    public static Result fillAccountProcess() {
	Form<FillAccountForm> fillAccountForm = sFillAccountForm.bindFromRequest();
        if (fillAccountForm.hasErrors()) {
            return badRequest(fillAccount.render(getUser(), fillAccountForm));
        }

	User user = getUser();
	user.account += Long.parseLong(fillAccountForm.field(User.FIELD_ACCOUNT).value());
	user.update();

	return redirect(routes.Application.profile());
    }


    @SubjectPresent
    public static Result createLot() {
	return ok(createLot.render(getUser(), sCreateLotForm));
    }

    @SubjectPresent
    public static Result addLot() {
	Form<CreateLotForm> createLotForm = sCreateLotForm.bindFromRequest();
        if (createLotForm.hasErrors()) {
            return badRequest(createLot.render(getUser(), createLotForm));
        }

	Long min = Long.parseLong(createLotForm.field(Lot.FIELD_MIN).value());
	User user = getUser();
	if(user.account < min) {
	    createLotForm.reject(Lot.FIELD_MIN, "У вас недостаточно средств. Вас счет должен быть больше минимальной суммы лота.");
            return badRequest(createLot.render(getUser(), createLotForm));
	}

	Lot lot = new Lot();
	lot.account = min;
	lot.min = min;


	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	try {
	     lot.start = formatter.parse(createLotForm.field(Lot.FIELD_START).value());
	     lot.end = formatter.parse(createLotForm.field(Lot.FIELD_END).value());
        } catch(ParseException e) {
	     // TODO: implement this case
        }

	lot.members = new HashSet<User>();
	lot.members.add(user);
	lot.owner = user;
	user.account -= lot.min;
	lot.save();
	user.update();

	return redirect(routes.Application.profile());
    }


    @SubjectPresent
    public static Result logout() {
	User user = getUser();
	if(user != null) {
	    user.hash = null;
	    user.ip = null;
	    user.update();
	    user = null;
	    session().clear();
	}
	return ok(index.render(null));
    }

    @SubjectPresent
    public static Result profile() {	
	return ok(profile.render(getUser()));
    }

    @Restrict({SecurityRole.ROLE_ADMIN})
    public static Result aprofile(String id) {	
	return ok(profile.render(User.findById(id)));
    }

    @Restrict({SecurityRole.ROLE_ADMIN})
    public static Result admin() {
	return ok(admin.render(getUser()));
    }

    @SubjectPresent
    public static Result getLotsPagesCount() {
        return ok(lotsPagesCount.render(Lot.allPages(5).getTotalPageCount()));	
    }

    @SubjectPresent
    public static Result takePart(String userid, String lotid) {
        User user = User.findById(userid);
        Lot lot = Lot.findById(lotid);

	user.account -= lot.min;
	lot.account  += lot.min;

        lot.getMembers().add(user);
        user.getMember_lots().add(lot);


	lot.update();
//	user.update();
	return ok(profile.render(user));
    }
   
    @SubjectPresent
    public static Result getLotsPage(Integer page) {
        PagingList<Lot> pagingList = Lot.allPages(5);
	Page<Lot> localLotsPage = pagingList.getPage(page);
	List<Lot> lotsList = localLotsPage.getList();
	return ok(lotsPage.render(lotsList));
    }

    @Restrict({SecurityRole.ROLE_ADMIN})
    public static Result getUsersPagesCount() {
        return ok(usersPagesCount.render(User.allPages(5).getTotalPageCount()));	
    }
   
    
    @Restrict({SecurityRole.ROLE_ADMIN})
    public static Result getUsersPage(Integer page) {
        PagingList<User> pagingList = User.allPages(5);	
	Page<User> localUsersPage = pagingList.getPage(page);
	List<User> usersList = localUsersPage.getList();
	return ok(usersPage.render(usersList));
    }

    public static Result javascriptRoutes() {
        response().setContentType("text/javascript");
        return ok(
            Routes.javascriptRouter("appJsRoutes",
               routes.javascript.Application.getUsersPage(),
               routes.javascript.Application.getUsersPagesCount(),
               routes.javascript.Application.getLotsPage(),
               routes.javascript.Application.getLotsPagesCount()
            )
        );
    }
}
