

# create it only if admin page in usage
class Admin extends Backbone.View
    events:
        "click .previous"  : "previousPage"
        "click .next"      : "nextPage"
    initialize: =>
        @curPage  = 0
        if @pageSize?
            @pageSize = 10
        appJsRoutes.controllers.Application.getUsersPagesCount().ajax
            context: this
            success: (data) ->
                @maxPages = parseInt data
                @updateTable(@curPage)
                @updatePaginator()
    previousPage: (e) =>
        e.preventDefault()
        if(@curPage > 0)
            @updateTable(--@curPage)
            @updatePaginator()
    nextPage: (e) =>
        e.preventDefault()
        if(@curPage + 1 < @maxPages)
            @updateTable(++@curPage)
            @updatePaginator()
    updateTable: (page) =>
        $(@el).find("table").find("tbody:last").children().remove()
        appJsRoutes.controllers.Application.getUsersPage(page).ajax
            context: this
            success: (data) ->
                $(@el).find("table").find("tbody:last").append data
    updateMaxPages: =>
        appJsRoutes.controllers.Application.getUsersPagesCount().ajax
            context: this
            success: (data) ->
                @maxPages = parseInt data
    updatePaginator: =>
        if(@curPage == 0)
            if(!$(@el).find(".previous").hasClass "disabled")
                $(@el).find(".previous").addClass "disabled"
        else
            if($(@el).find(".previous").hasClass "disabled")
               $(@el).find(".previous").removeClass "disabled"
        if(@curPage + 1 == @maxPages) 
            if(!$(@el).find(".next").hasClass "disabled")
                $(@el).find(".next").addClass "disabled"
        else
            if($(@el).find(".next").hasClass "disabled")
                $(@el).find(".next").removeClass "disabled"




# ------------------------------------- INIT APP
$ ->
    admin = new Admin
        el: $("#users_tab")
        pageSize: 20

