

# create it only if admin page in usage

class Profile extends Backbone.View
    events:
        "click .previous"  : "previousPage"
        "click .next"      : "nextPage"
    initialize: =>
        @curPage  = 0
        appJsRoutes.controllers.Application.getLotsPagesCount().ajax
            context: this
            success: (data) ->
                @maxPages = parseInt data
                @updateTable(@curPage)
                @updatePaginator()
    previousPage: (e) =>
        e.preventDefault()
        if(@curPage > 0)
            @updateTable(--@curPage)
            @updatePaginator()
    nextPage: (e) =>
        e.preventDefault()
        if(@curPage + 1 < @maxPages)
            @updateTable(++@curPage)
            @updatePaginator()
    updateTable: (page) =>
        $(@el).find("#lots_table > tbody:last").children().remove()
        appJsRoutes.controllers.Application.getLotsPage(page).ajax
            context: this
            success: (data) ->
                $(@el).find("#lots_table > tbody:last").append data
    updatePaginator: =>
        if(@curPage == 0)
            if(!$(@el).find(".previous").hasClass "disabled")
                $(@el).find(".previous").addClass "disabled"
        else
            if($(@el).find(".previous").hasClass "disabled")
               $(@el).find(".previous").removeClass "disabled"
        if(@curPage + 1 == @maxPages)
            if(!$(@el).find(".next").hasClass "disabled")
                $(@el).find(".next").addClass "disabled"
        else
            if($(@el).find(".next").hasClass "disabled")
                $(@el).find(".next").removeClass "disabled"



# ------------------------------------- INIT APP
$ ->
    profile = new Profile 
        el: $("#lots_container")


