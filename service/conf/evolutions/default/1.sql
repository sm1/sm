# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table lot (
  id                        bigint not null,
  owner_id                  bigint,
  account                   bigint not null,
  min                       bigint not null,
  start                     timestamp not null,
  end                       timestamp not null,
  constraint pk_lot primary key (id))
;

create table security_role (
  id                        bigint not null,
  name                      varchar(255) not null,
  constraint uq_security_role_name unique (name),
  constraint pk_security_role primary key (id))
;

create table user (
  id                        bigint not null,
  login                     varchar(255) not null,
  email                     varchar(255) not null,
  password                  varchar(255) not null,
  hash                      varchar(255),
  account                   bigint not null,
  ip                        varchar(255),
  constraint uq_user_login unique (login),
  constraint uq_user_email unique (email),
  constraint pk_user primary key (id))
;


create table lot_user (
  lot_id                         bigint not null,
  user_id                        bigint not null,
  constraint pk_lot_user primary key (lot_id, user_id))
;

create table user_security_role (
  user_id                        bigint not null,
  security_role_id               bigint not null,
  constraint pk_user_security_role primary key (user_id, security_role_id))
;
create sequence lot_seq;

create sequence security_role_seq;

create sequence user_seq;

alter table lot add constraint fk_lot_owner_1 foreign key (owner_id) references user (id) on delete restrict on update restrict;
create index ix_lot_owner_1 on lot (owner_id);



alter table lot_user add constraint fk_lot_user_lot_01 foreign key (lot_id) references lot (id) on delete restrict on update restrict;

alter table lot_user add constraint fk_lot_user_user_02 foreign key (user_id) references user (id) on delete restrict on update restrict;

alter table user_security_role add constraint fk_user_security_role_user_01 foreign key (user_id) references user (id) on delete restrict on update restrict;

alter table user_security_role add constraint fk_user_security_role_securit_02 foreign key (security_role_id) references security_role (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists lot;

drop table if exists lot_user;

drop table if exists security_role;

drop table if exists user;

drop table if exists user_security_role;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists lot_seq;

drop sequence if exists security_role_seq;

drop sequence if exists user_seq;

