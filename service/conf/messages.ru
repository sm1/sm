register=Регистрация
register.login=Логин
register.email=Почта
register.password=Пароль
register.repassword=Повтор пароля

login=Войти
login.login=Логин
login.password=Пароль

createLot=Создать лот

button.doLogin=Войти
button.doRegister=Отправить
button.goToRegister=Регистрация
button.createLot=Создать лот
button.fillAccount=Пополнить баланс

or=или

navbar.main=Главная
navbar.register=Регистрация
navbar.login=Войти
navbar.logout=Выйти
navbar.admin=Админка
navbar.createLot=Создать лот
navbar.fillAccount=Пополнить баланс

validation.format.email=Не верный формат электронной почты

validation.required=Поле должно быть заполнено
validation.required.start=Укажите дату начал лотереи
validation.required.end=Укажите дату окончания лотереи
validation.required.login=Логин должен быть заполнен
validation.required.email=Почта должна быть заполнена
validation.required.password=Пароль должен быть заполнен
validation.required.repassword=Повторите пароль

form.login.validation.invalid_user_or_pass=Не верный логин или пароль
form.create_lot.validation.invalid_account=Баланс должен быть числом
form.create_lot.validation.invalid_min=Минимальная сумма должна быть числом
form.create_lot.validation.invalid_cur_after_start=Дата начала лотереи должна быть позже текущей
form.create_lot.validation.invalid_end_after_start=Дата окончания лотереи должна быть позже даты начал лотереи
form.register.validation.login_exists=Логин уже занят
form.register.validation.email_exists=Почта уже занята
form.register.validation.pass_equals=Пароли должны совпадать

server.error=Ошибка сервера

profile.owner=Владелец
profile.lot_member=Вы являетесь участником лотов:
profile.lot_owner=Вы являетесь владельцем лотов:
profile.not_lot_member=Вы не являетесь участником ни одного лота.
profile.not_lot_owner=Вы не владеете ни одним лотом.
profile.lot_number=№
profile.lot_account=сумма
profile.lot_min=взнос для участия
profile.lot_start=дата начала
profile.lot_end=дата окончания
profile.lots_all=все лоты
profile.lots_owner=Мои лоты
profile.lots_member=Участие в лотах
profile.set_member=Принять участие

profile_nav.fill_account=Пополнить баланс
profile_nav.create_lot=Создать лот
profile_nav.account=Баланс: 

admin.users=Пользователи 

admin.user.id=Id
admin.user.login=Логин
admin.user.email=Почта
admin.user.account=Баланс
admin.user.actions=Действия

createLot.account=Баланс
createLot.min=Минимальная сумма для участия
createLot.start=Дата начала лотереи
createLot.end=Дата окончания лотереи

fillAccount=Пополнение баланса

fillAccount.account=Сумма

